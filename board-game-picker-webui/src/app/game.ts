export interface Game {
    _id: string;
    name: string;
    configuration: string;    
    duration: number;
    level: string;
    maxPlayers: number;
    minPlayers: number;
    notes: string;
    playedCounter: number;
    style: string;
  }

export interface GameRequest {
    configuration: string;    
    maxDuration: number;
    level: string;
    playersCount: number;    
    style: string;
  }

  export interface IGameResponse {
    total: number;
    results: Game[];
  }