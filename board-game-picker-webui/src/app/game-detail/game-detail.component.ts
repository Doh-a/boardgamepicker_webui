import { Component, OnInit, Input } from '@angular/core';
import {Location} from '@angular/common';
import {ActivatedRoute} from '@angular/router';

import { Game } from '../game';
import {GameService} from '../game.service';
import { MessageService } from '../messages.service';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.css']
})

export class GameDetailComponent implements OnInit {

  @Input() game?:Game;
  allowCancelLastPlay = false;

  constructor(
    private route: ActivatedRoute,
    private gameService: GameService,
    private messageService:MessageService,
    private location: Location   
  ) {     
    this.game = undefined;
    route.params.subscribe(val => {
      this.allowCancelLastPlay = false;
      this.getGame();
    })
  }

  ngOnInit(): void {
    
  }

  getGame(): void { 
    let stringId = this.route.snapshot.paramMap.get('id');
    if(stringId != null)
    {         
      this.gameService.getGame(stringId)
        .subscribe(game => this.game = game);
    }
  }

  goBack(): void {
    this.location.back();
  }

  playGame(): void {
    if(this.game !== undefined)
    {
      this.messageService.add("Currently playing " + this.game.name );
      this.gameService.playGame(this.game).subscribe(updatedPlayedCounter => 
        {
          this.game!.playedCounter = updatedPlayedCounter
          this.allowCancelLastPlay = true;
        });
    }
  }

  unplayGame(): void {
    if(this.game !== undefined)
    {
      this.messageService.add("Cancel playing " + this.game.name );
      this.gameService.unplayGame(this.game).subscribe(updatedPlayedCounter => 
        {
          this.game!.playedCounter = updatedPlayedCounter
          this.allowCancelLastPlay = false;
        });
    }
  }
}
