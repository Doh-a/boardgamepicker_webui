import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { Game } from '../game';
import {GameService} from '../game.service';

@Component({
  selector: 'app-add-game',
  templateUrl: './add-game.component.html',
  styleUrls: ['./add-game.component.css']
})
export class AddGameComponent implements OnInit {

  game ?: Game;

  insertForm = this.formBuilder.group({
    name: '',
    minPlayers : 0,
    maxPlayers : 0,
    duration : 0,
    configuration : "",
    level : "",
    style : ""
  });

  constructor(
    private gameService: GameService,
    private formBuilder : FormBuilder
  ) { }

  ngOnInit(): void {
  }

  onSubmit(): void {        
    this.game = this.insertForm.value;
    
    this.gameService.addGame(this.insertForm.value)
      .subscribe(game => console.warn('Your game has been added ', this.insertForm.value), error => console.error(error));
    // Process checkout data here    
    this.insertForm.reset();
  }

}
