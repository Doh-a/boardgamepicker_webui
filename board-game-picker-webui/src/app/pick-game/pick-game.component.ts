import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { GameService } from '../game.service';
import { Game } from '../game';

@Component({
  templateUrl: './pick-game.component.html',
  styleUrls: ['./pick-game.component.css']
})
export class PickGameComponent implements OnInit {

  pickGameForm = this.formBuilder.group({
    playersCount : 0,
    maxDuration : 0,
    configuration : "",
    level : "",
    style : ""
  });

  pickedGame ?: Game;

  constructor(
    private gameService: GameService,
    private formBuilder : FormBuilder
    ) { }

  ngOnInit(): void {
  }

  onSubmit(): void {            
    this.gameService.pickGame(this.pickGameForm.value)
      .subscribe(game => {
        if(game !== undefined)
          this.pickedGame = game;
        else
          console.log("No idea what you should play...")}
        , error => console.error(error));
  }

}
