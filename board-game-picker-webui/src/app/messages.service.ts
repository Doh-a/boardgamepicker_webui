import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MessageService {
  messages: string[] = [];
  private behaviorSubjectQueue: BehaviorSubject<string[]> = new BehaviorSubject<string[]>(this.messages);


  getInstance() : Observable<string[]> {
    return this.behaviorSubjectQueue;
  }

  add(message: string) {
    this.messages.push(message);
    this.behaviorSubjectQueue.next(this.messages);
  }

  clear() {
    this.messages = [];
    this.behaviorSubjectQueue.next(this.messages);
  }
}