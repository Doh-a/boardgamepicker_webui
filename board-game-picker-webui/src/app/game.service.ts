import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';


import { Game, GameRequest } from './game';
import { MessageService } from './messages.service';

@Injectable({
  providedIn: 'root'
})

export class GameService {

  private gamesApiUrl = 'http://188.166.29.107:8080';

  constructor(
    private http: HttpClient,
    private messageService:MessageService
  ) { }

  getGames(): Observable<Game[]> {
    return this.http.get<Game[]>(this.gamesApiUrl + '/games')
      .pipe(
        tap(_ => this.log('fetched games')),
        catchError(this.handleError<Game[]>('getGames', []))
      );    
  }

  getMostPlayedGames(): Observable<Game[]> {
    return this.http.get<Game[]>(this.gamesApiUrl + '/games?orderCriteria=playedCounter')
      .pipe(
        tap(_ => this.log('fetched games')),
        catchError(this.handleError<Game[]>('getGames', []))
      );    
  }

  getGame(id: string): Observable<Game|undefined> {    
    const url = `${this.gamesApiUrl}/gameById/${id}`;
    return this.http.get<Game>(url).pipe(
      tap(),
      catchError(this.handleError<Game>(`getGame id=${id}`))
    );
  }

  addGame(newGame : Game): Observable<Game|undefined> {    
    const url = `${this.gamesApiUrl}/insertGame`;
    return this.http.post<Game>(url, newGame).pipe(
      tap(_ => this.log(`add game ${newGame.name}`)),
      catchError(this.handleError<Game>(`add Game`, newGame))
    );
  }

  findGames(searchName:string) : Observable<Game[]> {
    const url = `${this.gamesApiUrl}/gamesByName/${searchName}`;
    return this.http.get<Game[]>(url).pipe(    
      catchError(this.handleError<Game[]>(`findGames`))
    );
  }

  pickGame(request : GameRequest): Observable<Game|undefined> {    
    const url = `${this.gamesApiUrl}/game`;
    
    return this.http.post<Game>(url, request).pipe(
      tap(newGame => { 
        this.log(`look for a game for ${request.playersCount} players.`);
        this.log("You should play " + newGame.name);
      }),
      catchError(this.handleError<Game>(`pickGame`))
    );
  }

  playGame(gamePlayed : Game) : Observable<number> {
    const url = `${this.gamesApiUrl}/gamePlayed/${gamePlayed._id}`;
    
    return this.http.get<number>(url).pipe(
      tap(playedCounter => { 
        this.log(gamePlayed.name + " has now been played " + playedCounter + " times.");        
      }),
      catchError(this.handleError<number>(`playGame`))
    )
  }

  unplayGame(gamePlayed : Game) : Observable<number> {
    const url = `${this.gamesApiUrl}/gameCancelPlayed/${gamePlayed._id}`;
    
    return this.http.get<number>(url).pipe(
      tap(playedCounter => { 
        this.log(gamePlayed.name + " has now been played " + playedCounter + " times.");        
      }),
      catchError(this.handleError<number>(`playGame`))
    )
  }

  private log(message: String) {
    this.messageService.add(`GameService: ${message}`);
  }

  /**
   * Handle Http operation that failed. 
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}, cause : ${error.error}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
