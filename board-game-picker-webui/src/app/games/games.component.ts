import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { from } from 'rxjs';
import { Game } from '../game';
import {GameService} from '../game.service'
import {MessageService} from '../messages.service';


@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.css']
})

export class GamesComponent implements OnInit {

  gamesServerList ?: Game[];
  games ?: Game[];
  filters = new Array<Filter>();
  maxDuration = 180;

  enableFilterPlayers = new FormControl(false);
  filterPlayers = new FormControl(false);
  enableFilterDuration = new FormControl(false);
  filterDuration = new FormControl(false);
  enableFilterLevel = new FormControl(false);
  filterLevel = new FormControl(false);
  enableFilterStyle = new FormControl(false);
  filterStyle = new FormControl(false);
  enableFilterConfiguration = new FormControl(false);
  filterConfiguration = new FormControl(false);

  panelOpenState = false;

  constructor(private gameService: GameService, private messageService: MessageService) { }

  ngOnInit(): void {
    this.getGames();

    this.enableFilterPlayers.valueChanges.subscribe(x => {      
      if(x && this.filterPlayers.value != false)
        this.addFilter("playersCount", this.filterPlayers.value);
      else
        this.removeFilter("playersCount");
    })

    this.filterPlayers.valueChanges.subscribe(x => {
      this.addFilter("playersCount", x);
   })

   this.enableFilterDuration.valueChanges.subscribe(x => {
    if(x && this.filterDuration.value != false)
      this.addFilter("duration", this.filterDuration.value);
    else
      this.removeFilter("duration");
  })

   this.filterDuration.valueChanges.subscribe(x => {
    this.addFilter("duration", x);
   })

    this.enableFilterLevel.valueChanges.subscribe(x => {
      if(x && this.filterLevel.value != false && this.filterLevel.value != "")
        this.addFilter("level", this.filterLevel.value);
      else
        this.removeFilter("level");
    })

    this.filterLevel.valueChanges.subscribe(x => {
      if(x != "")
      this.addFilter("level", x);
      else
        this.removeFilter("level");
    })

    this.enableFilterConfiguration.valueChanges.subscribe(x => {
      if(x && this.filterConfiguration.value != false && this.filterConfiguration.value != "")
        this.addFilter("configuration", this.filterConfiguration.value);
      else
        this.removeFilter("configuration");
    })

    this.filterConfiguration.valueChanges.subscribe(x => {
      if(x != "")
      this.addFilter("configuration", x);
      else
        this.removeFilter("configuration");
    })

    this.enableFilterStyle.valueChanges.subscribe(x => {
      if(x && this.filterStyle.value != false && this.filterStyle.value != "")
        this.addFilter("style", this.filterStyle.value);
      else
        this.removeFilter("style");
    })

    this.filterStyle.valueChanges.subscribe(x => {
      if(x != "")
      {
        this.addFilter("style", x);
      }
      else
        this.removeFilter("style");
    })
  }

  getGames() : void {
    this.gameService.getGames()
      .subscribe(games => {        
        this.gamesServerList = this.games = games;
        this.findMaxDuration();
      });
  }

  removeFilter(type : string) : void {
    let newFilters = new Array<Filter>();

    for(let filter of this.filters)
    {
      if(filter.type != type)
      {
        newFilters.push(filter);
      }
    }

    this.filters = newFilters;
    this.filterGames();
  }

  addFilter(type : string, value : any) : void {
    let filterFound = false;

    for(let filter of this.filters)
    {
      if(filter.type == type)
      {
        filter.value = value;
        filterFound = true;
      }
    }

    if(!filterFound)
      this.filters.push({type, value});

    this.filterGames();
  }

  filterGames() : void {
    if(this.gamesServerList !== undefined)
    {
      let filteredGames = this.gamesServerList;

      for(let filter of this.filters)
      {
        switch(filter.type)
        {
          case "playersCount":
            let newPlayersFilteredGames = new Array<Game>();            
            for(let game of filteredGames)
            {
              if(game.minPlayers <= filter.value && (game.maxPlayers === undefined || game.maxPlayers == 0 || game.maxPlayers >= filter.value))
              newPlayersFilteredGames.push(game);
            }
            filteredGames = newPlayersFilteredGames;
            break;
          case "duration":
              let newDurationFilteredGames = new Array<Game>();            
              for(let game of filteredGames)
              {
                if(game.duration <= filter.value)
                  newDurationFilteredGames.push(game);
              }
              filteredGames = newDurationFilteredGames;
              break;
          case "level":
                let newLevelFilteredGames = new Array<Game>();
                
                for(let game of filteredGames)
                {
                  let alreadyAdded = false;
                  for(let level of filter.value)                                    
                    if(!alreadyAdded && game.level == level)
                    {
                      alreadyAdded = true;
                      newLevelFilteredGames.push(game);
                    }
                }
                filteredGames = newLevelFilteredGames;
                break;
            case "configuration":
                  let newConfigurationFilteredGames = new Array<Game>();
                  
                  for(let game of filteredGames)
                  {
                    let alreadyAdded = false;
                    for(let configuration of filter.value)                                    
                      if(!alreadyAdded && game.configuration == configuration)
                      {
                        alreadyAdded = true;
                        newConfigurationFilteredGames.push(game);
                      }
                  }
                  filteredGames = newConfigurationFilteredGames;
                  break;
            case "style":
                  let newStyleFilteredGames = new Array<Game>();
                  
                  for(let game of filteredGames)
                  {
                    let alreadyAdded = false;
                    for(let style of filter.value)                                    
                      if(!alreadyAdded && game.style == style)
                      {
                        alreadyAdded = true;
                        newStyleFilteredGames.push(game);
                      }
                  }
                  filteredGames = newStyleFilteredGames;
                  break;
            default:
                  this.messageService.add(filter.type + " is not a managed filter");
              break;
        }
      }

      this.games = filteredGames;
    }
  }

  findMaxDuration() {
    if(this.games !== undefined)
    {
      this.maxDuration = 0;
      for(let game of this.games)
      {
        if(game.duration > this.maxDuration)
        {
          this.maxDuration = game.duration;
        }
      }
    }
  }

}

export interface Filter {
  type : string;
  value : any;
}
