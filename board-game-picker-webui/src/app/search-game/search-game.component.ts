import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { GameService } from '../game.service';
import { Game, IGameResponse } from '../game';
import { MessageService } from '../messages.service';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { startWith, map, debounceTime, switchMap, tap, finalize, filter } from 'rxjs/operators';

@Component({
  selector: 'app-search-game',
  templateUrl: './search-game.component.html',
  styleUrls: ['./search-game.component.css'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class SearchGameComponent implements OnInit {

  myControl = new FormControl();
  foundGames = new Array<Game>();
  filteredOptions?: Game[];
  options: string[] = ['Yamataï', 'Okiya', 'Himalaya', 'Aya'];

  lastSearchTerm = '';
  isLoading = false;

  constructor(
    private gameService: GameService,
    private messageService:MessageService,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.myControl.valueChanges.pipe(
      filter(value => value != ''),
      filter(value => value != this.lastSearchTerm),
      debounceTime(300),            
      tap(value => { this.lastSearchTerm = value; this.filteredOptions = undefined; this.isLoading = true; }),
      switchMap(value => this.gameService.findGames(value))
    ).subscribe(data => { if(data !== undefined) { this.filteredOptions = data; this.isLoading = false; }})
  }

  displayFn(game: Game): string {
    if (game) { return game.name; }
    return "";
  }

}
