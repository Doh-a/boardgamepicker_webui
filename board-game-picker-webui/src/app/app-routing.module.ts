import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GamesComponent } from './games/games.component';
import { AddGameComponent } from './add-game/add-game.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GameDetailComponent } from './game-detail/game-detail.component';
import { PickGameComponent } from './pick-game/pick-game.component';


const routes: Routes = [
  { path: 'games', component: GamesComponent },
  { path: 'addGame', component: AddGameComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'pickGame', component: PickGameComponent },
  { path : 'detail/:id', component:GameDetailComponent},
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }