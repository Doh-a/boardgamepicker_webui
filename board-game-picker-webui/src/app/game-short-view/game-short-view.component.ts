import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

import { Game } from '../game';
import { GameService } from '../game.service';
import { MessageService } from '../messages.service';

@Component({
  selector: 'app-game-short-view',
  templateUrl: './game-short-view.component.html',
  styleUrls: ['./game-short-view.component.css']
})
export class GameShortViewComponent implements OnInit {
  @Input() game : Game | undefined;

  constructor(
    private gameService: GameService,
    private messageService:MessageService
    ) { 
      
    }

  ngOnInit(): void {
    
  }

  playGame(e :  Event | undefined): void {
    e?.preventDefault();
    if(this.game !== undefined)
    {
      this.messageService.add("Currently playing " + this.game.name );
      this.gameService.playGame(this.game).subscribe(updatedPlayedCounter => 
        {
          this.game!.playedCounter = updatedPlayedCounter;
        });
    }
  }

  unplayGame(): void {
    if(this.game !== undefined)
    {
      this.messageService.add("Cancel playing " + this.game.name );
      this.gameService.unplayGame(this.game).subscribe(updatedPlayedCounter => 
        {
          this.game!.playedCounter = updatedPlayedCounter;
        });
    }
  }

}
