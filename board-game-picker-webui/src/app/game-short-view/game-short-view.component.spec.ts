import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameShortViewComponent } from './game-short-view.component';

describe('GameShortViewComponent', () => {
  let component: GameShortViewComponent;
  let fixture: ComponentFixture<GameShortViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameShortViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameShortViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
