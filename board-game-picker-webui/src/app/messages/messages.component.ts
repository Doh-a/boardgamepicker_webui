import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MessageService } from '../messages.service';


@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})

export class MessagesComponent implements OnInit {

  durationInSeconds = 2;
  messages = new Array<string>();
  
  constructor(
    private _snackBar: MatSnackBar,
    public messageService:MessageService
  ) { }

  ngOnInit(): void {
    this.messageService.getInstance().subscribe(val => {            
      if(val.length > 0)
      {
        let startDisplaySnackBar = false;

        if(this.messages.length == 0)
          startDisplaySnackBar = true;
        
          for(let value of val)
            this.messages.push(value);
        
          this.messageService.clear();

          if(startDisplaySnackBar)
            this.openSnackBar();
      }
    });
  }

  openSnackBar() {
    if(this.messages.length > 0)
    this._snackBar.open(this.messages[0], "ok", {
      duration: 1000,
    }).afterDismissed().subscribe(_ => {
      this.messages.shift();
      this.openSnackBar();
    });
  }
}
